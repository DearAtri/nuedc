import numpy as np
from PIL import Image
import onnxruntime
import cv2
import serial, time

"""
predict by onnx
"""

switch = 1
pic = True
flag = False
i = 0
j = 0
Cnt = 0
now = 0

buffer = bytes([170, 170, 0xFF, 0xFF, 0xCD, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x2b, 0xFF, 0xFF, 85, 85])
# bytes[11]: (prame2)    0x2b: 代表是nano发布的数据
# bytes[4]:  (fFormType) 0xCF: 表示没有找到目标物品 0xCE: 条形码 0xCD: 二维码
# bytes[5]:  (Location0) 0x00: 代表在图像中的第0位置。0x0n: 代表在图像中的第n位置。
ser = serial.Serial("/dev/ttyUSB0", 115200)

try:
    ser.open()
except:
    print("Serial is already open!")
finally:
    ser.write(buffer)

def detect(sess: onnxruntime.InferenceSession, input_: np.ndarray) -> int:
    """
    Args:
        sess: the InferenceSession of onnxruntime, which loads the onnx weight
        input_: np.array with the shape of (n, 3, 224, 224)
    Return:
        Officially prescribed format
    """
    # set the maps of the result to the competition's need
    results = sess.run(["output"], {"input": input_})
    # return results
    return int(np.argmax(results[0][0]))

def getModelSess(path):
    sess = onnxruntime.InferenceSession(path, providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
    return sess

def read_barcode(image):
    # 创建一个条形码识别器
    barcode_detector = cv2.BarcodeDetector_create()

    # 通过条形码识别器来检测条形码
    barcodes = barcode_detector.detect(image)

    # 如果检测到条形码，则返回True，否则返回False
    if len(barcodes) > 0:
        return True
    else:
        return False

def UART_Send(cls, pos):
    buf = bytes([170, 170, 0xFF, 0xFF, 0xCD+cls, 0x00+pos, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x2b, 0xFF, 0xFF, 85, 85])
    return buf

if __name__ == "__main__":
    cls_path = "./models/cls.onnx"
    pos_path = "./models/pos.onnx"

    cls_sess = getModelSess(cls_path)
    pos_sess = getModelSess(pos_path)
    
    # Create a video capture object for /dev/video1
    cap = cv2.VideoCapture('/dev/video0')
    
    # Check if video capture object was successfully opened
    if not cap.isOpened():
        print("Error opening video device")
        exit()
    ret, frame = cap.read()
    time.sleep(5)
    
    while True:
        # Read the frame from the video capture object
        ret, frame = cap.read()
        if j < 10:
            j += 1
            continue
        
        # Check if frame was successfully read
        if not ret:
            print("Error reading frame")
            break
        
        # Dis
        #frame = cv2.flip(frame, 1)
        # cv2.imshow('frame', frame)
        
        # Convert the frame to the required input format (n, 3, 224, 224)
        input_ = np.array([np.array(Image.fromarray(frame).resize((224, 224)))])
        input_ = input_.transpose((0, 3, 1, 2))
        input_ = input_.astype(np.float32) / 255
        
        # Perform inference
        cls_results = detect(cls_sess, input_)
        print(cls_results)

        pos_results = 0
        
        if cls_results == 1 and switch != 0:
            pos_results = detect(pos_sess, input_)
            print(pos_results)
            if pos_results == 1 or pos_results == 4 or pos_results == 7:
                ser.write(UART_Send(cls_results, pos_results))
                if cls_results == 1:
                    if flag == False:
                        time.sleep(3)
                        flag = True
                    for i in range(0,3):
                        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
                        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
                        ret, frame = cap.read()
                        cv2.imwrite("./save/qr0/qr-0000"+str(i)+".jpg", frame)
                        print("get pic!")
                    ser.write(UART_Send(3, pos_results))
                    break
        
        ser.write(UART_Send(cls_results, pos_results))
        
    # Release the video capture object and close any open windows
    cap.release()
    cv2.destroyAllWindows()
