#################################
# 此代码用于检测三角形、矩形和圆形 #
# By author xxn                 #
#################################


import cv2
import math
import numpy as np
from send import send_blob, get_serial

def extract_color(img, cnt):
    '''
    Args:
        img: The image to find color
        cnt: The edge of shape
    Return:
        blue_color(0, 0, 255) or red_color(255, 0, 0)
    '''
    mask = np.zeros(img.shape[:2], dtype=np.uint8)
    cv2.drawContours(mask, [cnt], -1, 255, -1)
    color = cv2.mean(img, mask=mask)[:3]  # 计算颜色均值
    color = np.array(color)  # 将颜色转换为NumPy数组
    
    # BGR
    blue_color = np.array([255, 0, 0])
    red_color = np.array([0, 0, 255])
    
    # Determine color (Blue or Red)
    blue_similarity = np.linalg.norm(color - blue_color)
    red_similarity = np.linalg.norm(color - red_color)
    
    if blue_similarity < red_similarity:
        return 'blue'
    else:
        return 'red'


def find_legal_blob(img, ser=None, min_rate=0.01, max_rate=0.4):
    '''
    Args:
        img: The image to find blobs.
        ser: The serial to send msg, set None to switch off.
    Return:
        ifFind: If find shape successfully, set True, else set False.
    '''
    buf = []
    point = []
    dis = int(math.sqrt(img.shape[0]**2 + img.shape[1]**2))
    send_buf = None

    send_colors = {'blue': 0x01, 'red': 0x02}
    send_shapes = {'Triangle': 0x01, 'Rectangle': 0x02, 'Circle': 0x03}

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 50, 255, 0)
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[1]

    low_area_case = min_rate * (img.shape[0] * img.shape[1])
    high_area_case = max_rate * (img.shape[0] * img.shape[1])
    low_y_case = 0.3 * img.shape[0]
    high_y_case = 0.7 * img.shape[0]
    low_x_case = 0.3 * img.shape[1]
    high_x_case = 0.7 * img.shape[1]
    remain_contours = []

    # Screen the cnt in range by area in (low_area_case, high_area_case)
    for cnt in contours:
        if cv2.contourArea(cnt) > low_area_case and cv2.contourArea(cnt) < high_area_case:
            remain_contours.append(cnt)
    
    print("Num of contours:", len(remain_contours))

    for cnt in remain_contours:
        perimeter = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.01 * perimeter, True)
    
        # Triangle
        if len(approx) == 3:
            M = cv2.moments(cnt)     
            if M['m00'] != 0.0:
                x = int(M['m10'] / M['m00'])
                y = int(M['m01'] / M['m00'])
                if x < low_x_case or x > high_x_case:
                    continue
                if y < low_y_case or y > high_y_case:
                    continue 
                point.append([x, y])
                img = cv2.drawContours(img, [cnt], -1, (0, 255, 255), 3) # 此代码用于在图上画出三角形轮廓
                cv2.putText(img, 'Triangle', (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0), 2) # 将Triangle文字打印于三角形上
                
                color = extract_color(img, cnt)
                if ser is not None:
                    send_color = send_colors[color]
                    buf.append(send_blob(send_color, send_shapes['Triangle'], x, y))
                print("Triangle Color:", color)

        # Rectangle
        elif len(approx) == 4:
            x, y, w, h = cv2.boundingRect(cnt)
            if x + w / 2 < low_x_case or x + w / 2 > high_x_case:
                continue
            if y + h / 2 < low_y_case or y + h / 2 > high_y_case:
                continue
            point.append([int(x + w / 2), int(y + h / 2)])
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 3)
            cv2.putText(img, 'Rectangle', (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)
            
            color = extract_color(img, cnt)
            if ser is not None:
                send_color = send_colors[color]
                buf.append(send_blob(send_color, send_shapes['Rectangle'], int(x + w / 2), int(y + h / 2)))
            print("Rectangle Color:", color)

        # Circle
        elif len(approx) > 10:
            center, radius = cv2.minEnclosingCircle(cnt)
            if center[0] < low_x_case or center[0] > high_x_case:
                continue
            if center[1] < low_y_case or center[1] > high_y_case:
                continue
            point.append([int(center[0]), int(center[1])])
            img = cv2.circle(img, (int(center[0]), int(center[1])), int(radius), (255, 0, 0), 3)
            cv2.putText(img, 'Circle', (int(center[0]), int(center[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)
            
            color = extract_color(img, cnt)
            if ser is not None:
                send_color = send_colors[color]
                buf.append(send_blob(send_color, send_shapes['Circle'], int(center[0]), int(center[1])))
            print("Circle Color:", color)
    if ser is None:
        cv2.imshow("Shapes", img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    if len(buf) == 0:
        return send_buf, False
    else:
        for i in range(len(buf)):
            temp_dis = int(math.sqrt((img.shape[0] / 2 - point[i][0])**2 + (img.shape[1] / 2 - point[i][1])**2))
            if dis > temp_dis:
                dis = temp_dis
                send_buf = buf[i]
    
    return send_buf, True

if __name__ == "__main__":
    ser = get_serial("/dev/ttyUSB0", 115200)
    # Begin --- open camera
    cap = cv2.VideoCapture('/dev/video0')
    if not cap.isOpened():
        print("Error opening video device")
        exit()
    # End

    while (True):
        # Begin --- read from camera
        ret, frame = cap.read()
        if not ret:
            print("Error reading frame")
            break
        # End
        buf, isFind = find_legal_blob(frame, ser)
        if isFind is False:
            ser.write(send_blob(0x00, 0x00, 0, 0))