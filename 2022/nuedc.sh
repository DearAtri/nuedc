#! /bin/bash

# by author xxn

# Find T265
rs-enumerate-devices > /home/amov/rs.log
error=$(cat /home/amov/rs.log)
while true; do
        if [ "$error" = "No device detected. Is it plugged in?" ]; then
                rm /home/amov/rs.log
                rs-enumerate-devices > /home/amov/rs.log
                error=$(cat /home/amov/rs.log)
        else
                break
        fi
done
echo "$error"
rm /home/amov/rs.log

source realsense_ws/devel/setup.bash
nohup roslaunch realsense2_camera rs_t265.launch &
source serial_ws/devel/setup.bash
rosrun tx_pkg odom_send.py
