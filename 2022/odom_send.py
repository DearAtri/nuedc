#! /usr/bin/python3

import serial
import cv2
import math
import time
import numpy as np
import rospy
from nav_msgs.msg import Odometry
from scipy.signal import firwin, lfilter
from KF import Kalman

out = None
background_subtractor = cv2.createBackgroundSubtractorMOG2()
point = [0, 0]
rx = 0
ry = 0
count = 0

def extract_color(img, cnt):
    '''
    Args:
        img: The image to find color
        cnt: The edge of shape
    Return:
        blue_color(0, 0, 255) or red_color(255, 0, 0)
    '''
    mask = np.zeros(img.shape[:2], dtype=np.uint8)
    cv2.drawContours(mask, [cnt], -1, 255, -1)
    color = cv2.mean(img, mask=mask)[:3]  # 计算颜色均值
    color = np.array(color)  # 将颜色转换为NumPy数组

    # BGR
    blue_color = np.array([255, 0, 0])
    red_color = np.array([0, 0, 255])

    # Determine color (Blue or Red)
    blue_similarity = np.linalg.norm(color - blue_color)
    red_similarity = np.linalg.norm(color - red_color)

    if blue_similarity < red_similarity:
        return 'blue'
    else:
        return 'red'


def find_legal_blob(img, min_rate=0.01, max_rate=0.4, little_endian=False):
    '''
    Args:
        img: The image to find blobs.
        ser: The serial to send msg, set None to switch off.
    Return:
        ifFind: If find shape successfully, set True, else set False.
    '''
    buf = []
    point = []
    dis = int(math.sqrt(img.shape[0]**2 + img.shape[1]**2))
    send_buf = None

    send_colors = {'blue': 0x01, 'red': 0x02}
    send_shapes = {'Triangle': 0x01, 'Rectangle': 0x02, 'Circle': 0x03}

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 50, 255, 0)
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[1]

    low_area_case = min_rate * (img.shape[0] * img.shape[1])
    high_area_case = max_rate * (img.shape[0] * img.shape[1])
    low_y_case = 0.2 * img.shape[0] # 480
    high_y_case = 0.8 * img.shape[0]
    low_x_case = 0.2 * img.shape[1] # 640
    high_x_case = 0.8 * img.shape[1]
    remain_contours = []

    # print(high_area_case)
    # Screen the cnt in range by area in (low_case, high_case)
    for cnt in contours:
        if cv2.contourArea(cnt) * 2 > low_area_case and cv2.contourArea(cnt) * 2 < high_area_case:
            remain_contours.append(cnt)

    for cnt in remain_contours:
        perimeter = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.01 * perimeter, True)

        # Triangle
        if len(approx) == 3:
            M = cv2.moments(cnt)
            if M['m00'] != 0.0:
                x = int(M['m10'] / M['m00'])
                y = int(M['m01'] / M['m00'])
                if x < low_x_case or x > high_x_case:
                    continue
                if y < low_y_case or y > high_y_case:
                    continue
                point.append([x, y])
                img = cv2.drawContours(img, [cnt], -1, (0, 255, 255), 3) # 此代码用于在图上画出三角形轮廓
                cv2.putText(img, 'Triangle', (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0), 2) # 将Triangle文字打印于三角形上

                color = extract_color(img, cnt)
                send_color = send_colors[color]
                buf.append(send_blob(send_color, send_shapes['Triangle'], x, y, little_endian=little_endian))
                print("Triangle Color:", color)

        # Rectangle
        elif len(approx) == 4:
            x, y, w, h = cv2.boundingRect(cnt)
            if x + w / 2 < low_x_case or x + w / 2 > high_x_case:
                continue
            if y + h / 2 < low_y_case or y + h / 2 > high_y_case:
                continue
            # area = w * h
            # if area < low_area_case or area > high_area_case:
            #     continue
            point.append([int(x + w / 2), int(y + h / 2)])
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 3)
            cv2.putText(img, 'Rectangle', (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

            color = extract_color(img, cnt)
            send_color = send_colors[color]
            buf.append(send_blob(send_color, send_shapes['Rectangle'], int(x + w / 2), int(y + h / 2), little_endian=little_endian))
            print("Rectangle Color:", color)

        # Circle
        elif len(approx) > 4:
            center, radius = cv2.minEnclosingCircle(cnt)
            if center[0] < low_x_case or center[0] > high_x_case:
                continue
            if center[1] < low_y_case or center[1] > high_y_case:
                continue
            # area = 3.1415 * radius * radius
            # if area < low_area_case or area > high_area_case:
            #     continue
            point.append([int(center[0]), int(center[1])])
            img = cv2.circle(img, (int(center[0]), int(center[1])), int(radius), (255, 0, 0), 3)
            cv2.putText(img, 'Circle', (int(center[0]), int(center[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)

            color = extract_color(img, cnt)
            send_color = send_colors[color]
            buf.append(send_blob(send_color, send_shapes['Circle'], int(center[0]), int(center[1]), little_endian=little_endian))
            print("Circle Color:", color, cv2.contourArea(cnt))

    if len(buf) == 0:
        return send_buf, False
    else:
        cv2.imwrite('./save.png', img)
        for i in range(len(buf)):
            temp_dis = int(math.sqrt((img.shape[0] / 2 - point[i][0])**2 + (img.shape[1] / 2 - point[i][1])**2))
            if dis > temp_dis:
                dis = temp_dis
                send_buf = buf[i]

    return send_buf, True

def find_car(img, min_rate=0.003, max_rate=0.4):
    global out, rx, ry, count, kalman
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 50, 255, 0)
    thresh = cv2.GaussianBlur(thresh, (3,3), 1)
    cv2.imwrite("./bin.jpg", thresh)
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[1]
    low_case = min_rate * (img.shape[0] * img.shape[1])
    high_case = max_rate * (img.shape[0] * img.shape[1])
    remain_contours = []

    wheels = []

    # Screen the cnt in range by area in (low_case, high_case)
    for cnt in contours:
        if cv2.contourArea(cnt) > low_case and cv2.contourArea(cnt) < high_case:
            remain_contours.append(cnt)
    print("Num of contours:", len(contours))
    for cnt in remain_contours:
        approx = cv2.approxPolyDP(cnt, 0.01*cv2.arcLength(cnt, True), True)
        if len(approx) > 20:
            center, radius = cv2.minEnclosingCircle(cnt)
            wheels.append([int(center[0]), int(center[1])])
            img = cv2.circle(img, (int(center[0]), int(center[1])), int(radius), (255, 0, 0), 3)
            #cv2.putText(img, 'Circle', (int(center[0]), int(center[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)
            text=str(int(center[0]))+' '+str(int(center[1]))
            cv2.putText(img,text,(int(center[0]), int(center[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
   # out.write(img)
    FormType = 110
    if len(wheels) != 0:
        wheels = np.array(wheels)
        car_point = wheels.mean(axis=0)
        rx = int(car_point[0])
        ry = int(car_point[1])
        count = 0
    else:
        count += 1
        if count == 30:
            count = 0
            FormType = 0

    (px, py) = kalman.predict_update([rx, ry])
    return send_XY(px, py, FormType=FormType), True

def find_bg(img, min_rate=0.03, max_rate=0.4):
    global out, background_subtractor
    fg_mask = background_subtractor.apply(img)

    low_area_case = min_rate * (img.shape[0] * img.shape[1])
    high_area_case = max_rate * (img.shape[0] * img.shape[1])
    # 在原图上绘制前景物体
    # frame_with_fg = cv2.bitwise_and(img, img, mask=fg_mask)

    contours = cv2.findContours(fg_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]
    Points = []

    # 遍历轮廓
    for contour in contours:
        if cv2.contourArea(contour) < low_area_case or cv2.contourArea(contour) > high_area_case:
            continue
        # 计算轮廓的图像矩
        M = cv2.moments(contour)

        print(cv2.contourArea(contour))

        if (M['m00'] == 0):
            continue
        # 计算轮廓的中心坐标
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        Points.append([cx, cy])
        try:
            send_XY(cx, cy)
        except:
            pass
        # 在原图上绘制中心坐标
        cv2.circle(img, (cx, cy), 5, (0, 255, 0), -1)
        if out is not None:
            out.write(img)
    if len(Points) == 0:
        return send_XY(0, 0), False
    Points = np.array(Points)
    point = Points.mean(axis=0)
    car_x = int(point[0])
    car_y = int(point[1])
    return send_XY(car_x, car_y, FormType=110), True

def get_serial(srl='/dev/ttyUSB0', br=115200):
    '''
    Args:
        srl: Serial device
        br: baund rate, default with 115200
    Return:
        ser: use to send serial msg
    Exit:
        0: permission denied
        -1: init failed
        -2: open failed
    '''
    try:
        ser = serial.Serial(srl, br)
    except serial.serialutil.SerialException as e:
        if e.errno == 13:
            print("Please use command in Terminal:\n\tsudo chmod 777 %s" % (srl))
            print("Or if you want change your permission permanently, use below command:\n\tsudo chmod -a -G dialout {username}")
            print("Exit code 0!")
            exit(0)
        elif e.errno == 2:
            print("Serial init failed!\nExit code -1!")
            exit(-1)

    try:
        ser.open()
    except:
        pass
    finally:
        if ser.isOpen():
            print("Serial is already!")
        else:
            print("Serial open failed!\nExit code -2!")
            exit(-2)

    return ser

def send_XY(x, y, FormType=0, little_endian=False):
    '''
    Args:
        ser: Got from get_serial
        x, y: Point of drone (int16)
        little_endian: Set True to reverse array
    Returns:
        buf: bytes array.
    '''
    return send_base(FormType, x, y, little_endian=little_endian)

def send_base(FormType, param1=0, param2=0, param3=None, param4=None, param5=None, param6=None , head=0xFF, tail=0x55, little_endian=False):
    '''
    Args:
        ser: Got from get_serial
        FormType: Defined by authors.
        param1 & param2: int16
        param3 & param3: Set num to use
        head & tail: Defined by authors.
    Returns:
        buf: bytes array.
    '''
    buf = bytes([head, head, FormType & 0xFF, param1 & 0xFF, (param1 >> 8) & 0xFF, param2 & 0xFF, (param2 >> 8) & 0xFF])
    if param3 is not None:
        buf += bytes([(param3 >> 8) & 0xFF, param3 & 0xFF])
        if param4 is not None:
            buf += bytes([(param4 >> 8) & 0xFF, param4 & 0xFF])
            if param5 is not None:
                buf += bytes([(param5 >> 8) & 0xFF, param5 & 0xFF])
                if param6 is not None:
                    buf += bytes([(param6 >> 8) & 0xFF, param6 & 0xFF])
    buf += bytes([tail, tail])
    if little_endian:
        buf = buf[::-1]

    return buf


def concat(buf1, buf2, tail=0x55, little_endian=False):
    '''
    Args:
        buf1 & buf2: The buf to concat, got from send_base.
        tail: Buf1 and buf2 must have same tail.
        little_endian: Set True to reverse buf.
    Returns:
        buf: [head, buf1.param, buf2.param, tail]
    '''
    buf = buf1[:-2] + buf2[2:]
    return buf
    for i in range(2, len(buf1)):
        if buf1[i] != tail:
            buf += bytes([buf1[i] & 0xFF])
        else:
            break
    for i in range(2, len(buf2)):
        if buf2[i] != tail:
            buf += bytes([buf2[i] & 0xFF])
        else:
            break
    buf += buf1[-2:]
    return buf

def send_blob(color, shape, x, y, little_endian=False):
    '''
    Args:
        ser: Got from get_serial
        color: 0x01: Blue  0x02: Red
        shape: 0x01: Triangle
               0x02: Rectangle
               0x03: Circle
        x, y: Center point of shape (int16)
        little_endian: Set True to reverse array
    Hint:
        If color_shape equal 0, that means not found.
        FormType: high 8 bits means color, low 8 bits means shape
    Returns:
        buf: bytes array.
    '''
    FormType = (color << 4) + shape
    return send_base(FormType, x, y, little_endian=little_endian)

temp_x = 0
temp_y = 0
speed_x = 0
speed_y = 0
old_x = 0
old_y = 0
old_time = 0
speed_xs = [0] * 50
speed_ys = [0] * 50
# 定义滤波器参数
cutoff_freq = 0.8  # 截止频率
num_taps = 50  # 滤波器长度
filter_coefficients = firwin(num_taps, cutoff_freq)
cap = cv2.VideoCapture('/dev/video0')
kalman = Kalman()

def send_Pose(data):
    global temp_x, temp_y, cap, speed_x, speed_y, old_x, old_y, old_time, filter_coefficients
    x=int(data.pose.pose.position.x*1000)
    y=int(data.pose.pose.position.y*1000)
    now = time.time() * 1000
    if abs(x-temp_x)>800:
        x = temp_x
    else:
        temp_x = x
    if abs(y-temp_y)>800:
        y=temp_y
    else:
        temp_y = y
    buf1 = send_XY(x, y)
    if not cap.isOpened():
        print("tran to video0")
        cap = cv2.VideoCapture('/dev/video0')
        if not cap.isOpened():
            print("tran to video1")
            cap = cv2.VideoCapture('/dev/video1')
    ret, frame = cap.read()
    if not ret:
        isFind = False
    else:
        buf2, isFind = find_car(frame)
        # buf2, isFind = find_legal_blob(frame)
    if isFind == False:
        buf2 = send_blob(0, 0, 0, 0)
    buf = concat(buf1, buf2)
    buf = bytearray(buf)
    if old_x == 0 and old_y == 0:
        pass
    else:
        speed_x = int((x - old_x) * 100 / (now - old_time))
        speed_y = int((y - old_y) * 100 / (now - old_time))
        speed_xs.pop(0)
        speed_xs.append(speed_x)
        speed_ys.pop(0)
        speed_ys.append(speed_y)
        speed_x = int(np.mean(lfilter(filter_coefficients, 1, speed_xs)))
        speed_y = int(np.mean(lfilter(filter_coefficients, 1, speed_ys)))
    # print(old_x, old_y, x, y, old_time, now, speed_x, speed_y)
    speed = send_XY(speed_x, speed_y)
    buf = concat(buf, speed)
    old_x = x
    old_y = y
    old_time = now
    ser.write(buf)
    print(buf)

if __name__ == "__main__":
    import os
    pid = os.getpid()
    with open("/home/tx2/pid.txt", "w") as f:
        f.write(str(pid))
    rospy.init_node("sub_odom_p")
    while True:
        try:
            ser = get_serial("/dev/ttyUSB0")
            break
        except:
            pass
    if not cap.isOpened():
        print("Error opening video0!")
        cap = cv2.VideoCapture('/dev/video1')
        if not cap.isOpened():
            print("Error oppening video1!")
            exit()
        else:
            print("success")
    else:
        print("success")
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter("./output.avi", fourcc, 30, (640, 480), True)
    sub = rospy.Subscriber("/camera/odom/sample",Odometry,send_Pose,queue_size=10)
    buf = rospy.spin()
