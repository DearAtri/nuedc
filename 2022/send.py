#################################
# 用于串口通信                   #
# By author xxn                 #
#################################

import serial

def get_serial(srl='/dev/ttyUSB0', br=115200):
    '''
    Args:
        srl: Serial device
        br: baund rate, default with 115200
    Return:
        ser: use to send serial msg
    Exit:
        0: permission denied
        -1: init failed
        -2: open failed
    '''
    try:
        ser = serial.Serial(srl, br)
    except serial.serialutil.SerialException as e:
        if e.errno == 13:
            print("Please use command in Terminal:\n\tsudo chmod 777 %s" % (srl))
            print("Or if you want change your permission permanently, use below command:\n\tsudo chmod -a -G dialout {username}")
            print("Exit code 0!")
            exit(0)
        elif e.errno == 2:
            print("Serial init failed!\nExit code -1!")
            exit(-1)

    try:
        ser.open()
    except:
        pass
    finally:
        if ser.isOpen():
            print("Serial is already!")
        else:
            print("Serial open failed!\nExit code -2!")
            exit(-2)

    return ser

def send_base(FormType, param1=0, param2=0, param3=None, param4=None, param5=None, param6=None head=0xFF, tail=0x55, little_endian=False):
    '''
    Args:
        ser: Got from get_serial
        FormType: Defined by authors.
        param1 & param2: int16
        param3 & param3: Set num to use
        head & tail: Defined by authors.
    Returns:
        buf: bytes array.
    '''
    buf = bytes([head, head, FormType & 0xFF, param1 & 0xFF, (param1 >> 8) & 0xFF, param2 & 0xFF, (param2 >> 8) & 0xFF])
    if param3 is not None:
        buf += bytes([param3 & 0xFF, (param3 >> 8) & 0xFF])
        if param4 is not None:
            buf += bytes([param4 & 0xFF, (param4 >> 8) & 0xFF])
            if param5 is not None:
                buf += bytes([param5 & 0xFF, (param5 >> 8) & 0xFF])
                if param6 is not None:
                    buf += bytes([param6 & 0xFF, (param6 >> 8) & 0xFF])
    buf += bytes([tail, tail])
    if little_endian:
        buf = buf[::-1]

    return buf

def send_XY(x, y, little_endian=False):
    '''
    Args:
        ser: Got from get_serial
        x, y: Point of drone (int16)
        little_endian: Set True to reverse array
    Returns:
        buf: bytes array.
    '''
    return send_base(0, x, y, little_endian)

def send_blob(color, shape, x, y, little_endian=False):
    '''
    Args:
        ser: Got from get_serial
        color: 0x01: Blue  0x02: Red
        shape: 0x01: Triangle
               0x02: Rectangle
               0x03: Circle
        x, y: Center point of shape (int16)
        little_endian: Set True to reverse array
    Hint:
        If color_shape equal 0, that means not found.
        FormType: high 8 bits means color, low 8 bits means shape
    Returns:
        buf: bytes array.
    '''
    FormType = (color << 4) + shape
    return send_base(FormType, x, y, little_endian)

def concat(buf1, buf2, tail=0x55):
    '''
    Args:
        buf1 & buf2: The buf to concat, got from send_base.
        tail: Buf1 and buf2 must have same tail.
        little_endian: Set True to reverse buf.
    Returns:
        buf: [head, buf1.param, buf2.param, tail]
    '''
    buf = buf1[:2]
    for i in range(2, len(buf1)):
        if buf1[i] != tail:
            buf += bytes([buf1[i]])
        else:
            break
    for i in range(2, len(buf2)):
        if buf2[i] != tail:
            buf += bytes([buf2[i]])
        else:
            break
    buf += buf1[-2:]
    return buf

def send_tx2(X, Y, color, shape, x, y, little_endian=False, offset=0x64):
    '''
    Args:
        ser: Got from get_serial
        X, Y: Point of drone (int16)
        color: 0x01: Blue  0x02: Red
        shape: 0x01: Triangle
               0x02: Rectangle
               0x03: Circle
        x, y: Center point of shape (int16)
        little_endian: Set True to reverse array
    Hint:
        offset: 0x64
        Concat the msgs of depth camera and color-shape.
    Returns:
        buf: bytes array.
    '''
    color_shape = offset + (color << 4) + shape
    return send_base(FormType, X, Y, x, y, little_endian=little_endian)

if __name__ == "__main__":
    ser = get_serial("/dev/tty50")
    point = send_XY(0, 0, little_endian=True)
    blob = send_blob(0x01, 0x02, 0, 0, little_endian=True)
    ser.write(concat(point, blob, little_endian=True))