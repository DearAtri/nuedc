import cv2
import numpy as np

class Kalman:
    """
    Abstract:
        Input the sequences and predict with KF.
    """
    def __init__(self):
        global measurement, process_noise, font
        stateNum = 4
        measureNum = 2
        self.kalman = cv2.KalmanFilter(stateNum, measureNum, 0)
        self.process_noise = np.zeros((stateNum, 1), np.float32)
        self.measurement = np.zeros((measureNum, 1), np.float32)

        A = np.array([[1, 0, 1, 0],
                    [0, 1, 0, 1],
                    [0, 0, 1, 0],
                    [0, 0, 0, 1]], np.float32)

        self.kalman.transitionMatrix = A
        self.kalman.measurementMatrix = np.eye(measureNum, stateNum, dtype=np.float32)
        self.kalman.processNoiseCov = np.eye(stateNum, dtype=np.float32) * 1e-5
        self.kalman.measurementNoiseCov = np.eye(measureNum, dtype=np.float32) * 1e-1
        self.kalman.errorCovPost = np.eye(stateNum, dtype=np.float32) * 1
        self.kalman.statePost = np.random.uniform(0, 640, (stateNum, 1)).astype(np.float32)
    
    def predict_update(self, car_position):
        '''
        Args:
            car_position: The center point (x, y) of car.
        Returns:
            predict_pt: The prediction of KF.
        '''
        prediction = self.kalman.predict()
        predict_pt = (int(prediction[0, 0]), int(prediction[1, 0]))
        self.measurement[0, 0] = float(car_position[0])
        self.measurement[1, 0] = float(car_position[1])
        self.kalman.correct(self.measurement)
        return predict_pt
    
if __name__ == "__main__":
    kalman = Kalman()
    cp = (10, 10)
    cp_predict = kalman.predict_update(cp)