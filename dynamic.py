import cv2
import numpy as np
try:
    from send import send_XY
except:
    pass

low_area_case = 0.
high_area_case = 0.

def find_dynamic(cap, min_rate=0.01, max_rate=0.4, out=None):
    ret, frame = cap.read()
    low_area_case = min_rate * (frame.shape[0] * frame.shape[1])
    high_area_case = max_rate * (frame.shape[0] * frame.shape[1])
    background_subtractor = cv2.createBackgroundSubtractorMOG2()
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        
        # 背景减除，得到前景掩码
        fg_mask = background_subtractor.apply(frame)
        
        # 对前景掩码进行处理，如二值化，腐蚀、膨胀等操作，以得到更好的结果
        
        # 在原图上绘制前景物体
        frame_with_fg = cv2.bitwise_and(frame, frame, mask=fg_mask)

        contours = cv2.findContours(fg_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]
        
        # 遍历轮廓
        for contour in contours:
            if cv2.contourArea(contour) < low_area_case or cv2.contourArea(contour) > high_area_case:
                continue
            # 计算轮廓的图像矩
            M = cv2.moments(contour)

            print(cv2.contourArea(contour))

            if (M['m00'] == 0):
                continue
            # 计算轮廓的中心坐标
            cx = int(M['m10'] / M['m00'])
            cy = int(M['m01'] / M['m00'])
            try:
                send_XY(cx, cy)
            except:
                pass
            # 在原图上绘制中心坐标
            cv2.circle(frame_with_fg, (cx, cy), 5, (0, 255, 0), -1)
        if out is not None:
            out.write(frame_with_fg)
        # 显示结果
        cv2.imshow('Foreground', frame_with_fg)
        
        if cv2.waitKey(30) & 0xFF == 27:  # 按'Esc'键退出
            break
    cap.release()
    if out is not None:
        out.release()
    cv2.destroyAllWindows()

def find_bg(img, min_rate=0.01, max_rate=0.4, out=None):
    fg_mask = background_subtractor.apply(img)
        
    low_area_case = min_rate * (img.shape[0] * img.shape[1])
    high_area_case = max_rate * (img.shape[0] * img.shape[1])
    # 在原图上绘制前景物体
    # frame_with_fg = cv2.bitwise_and(img, img, mask=fg_mask)

    contours = cv2.findContours(fg_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]
    Points = []

    # 遍历轮廓
    for contour in contours:
        if cv2.contourArea(contour) < low_area_case or cv2.contourArea(contour) > high_area_case:
            continue
        # 计算轮廓的图像矩
        M = cv2.moments(contour)

        print(cv2.contourArea(contour))

        if (M['m00'] == 0):
            continue
        # 计算轮廓的中心坐标
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        Points.append([cx, cy])
        try:
            send_XY(cx, cy)
        except:
            pass
        # 在原图上绘制中心坐标
        cv2.circle(img, (cx, cy), 5, (0, 255, 0), -1)
        if out is not None:
            out.write(img)
    if len(Points) == 0:
        return send_XY(0, 0), False
    Points = np.array(Points)
    point = Points.mean(axis=0)
    car_x = int(point[0])
    car_y = int(point[1])
    return send_XY(car_x, car_y, FormType=110), True

if __name__ == "__main__":
    cap = cv2.VideoCapture('car.mp4')
    ret, frame = cap.read()
    output_file = 'output.mp4'
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    fps = 30.0
    output_size = (frame.shape[1], frame.shape[0])
    out = cv2.VideoWriter(output_file, fourcc, fps, output_size)
    find_dynamic(cap, out=out)