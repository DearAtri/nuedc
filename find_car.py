from send import *
import cv2
import numpy as np

def find_car(img, min_rate=0.003, max_rate=0.01):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 50, 255, 0)
    cv2.imwrite('bin.jpg', thresh)
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[1]
    low_case = min_rate * (img.shape[0] * img.shape[1])
    high_case = max_rate * (img.shape[0] * img.shape[1])
    remain_contours = []

    wheels = []

    # Screen the cnt in range by area in (low_case, high_case)
    for cnt in contours:
        if cv2.contourArea(cnt) > low_case and cv2.contourArea(cnt) < high_case:
            remain_contours.append(cnt)
    print("Num of contours:", len(contours))
    for cnt in remain_contours:
        approx = cv2.approxPolyDP(cnt, 0.01*cv2.arcLength(cnt, True), True)
        if len(approx) > 10:
            center, radius = cv2.minEnclosingCircle(cnt)
            wheels.append([int(center[0]), int(center[1])])
    if len(wheels) == 0:
        return None

    wheels = np.array(wheels)
    print(wheels)
    car_point = wheels.mean(axis=0)
    car_x = int(car_point[0])
    car_y = int(car_point[1])

    return car_x, car_y

if __name__ == "__main__":
    img = cv2.imread("./car.jpg")
    x, y = find_car(img)
    cv2.putText(img, 'Car', (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 2)
    cv2.imshow("car", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()